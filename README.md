# Spring IoT

* Author: __João Faustino__
* Date: __2024-02-14__


## Deploy

### Local run

__windows__
    mvnw.cmd spring-boot:run

__linux__
    ./mvnw spring-boot:run

> NOTE: `JAVA_HOME` must be declared on user variables

    echo %JAVA_HOME%

### Local Build

```
mvnm.cmd clean
mvnm.cmd package
```

## Run
    java -jar target/<app>0.0.1-SNAPSHOT.jar

## Activate profiles

### with environment variables
__Windows__
    set spring_profiles_active=<profile to be used>

__Linux__
    export spring_profiles_active=<profile to be used>

### From maven ops

    mvnw.cmd spring-boot:run -Dspring-boot.run.profiles=development

### From Java ops
    java -jar target/<app>-0.0.1-SNAPSHOT.jar --spring.profiles.active=production

### Test
    mvnw.cmd test


### Maven Code Coverage
    mvnw.cmd verify

### Join ubuntu
    ssh -i ~/.ssh/ubuntu-key-20220301.rsa ubuntu@34.78.113.223