package pt.deloitte.springiot.simulator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class SimulatorComponent {

    @Value("${simulator.max_random}")
    private int maxRandom;

    @Value("${simulator.blink_millis}")
    private int blinkMillis;
    
}
