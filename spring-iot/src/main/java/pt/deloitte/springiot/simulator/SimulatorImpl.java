package pt.deloitte.springiot.simulator;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.annotations.Expose;

import lombok.ToString;

@Service
@ToString
public class SimulatorImpl implements Simulator {
    
    @Autowired
private SimulatorComponent setup;

    @Expose
    private boolean blink = false;
    @Expose
    private final AtomicInteger counter = new AtomicInteger();
    @Expose
    private float random;

    private long lastBlinkTime;

    public SimulatorImpl(){
        this.lastBlinkTime= System.currentTimeMillis();
    }


    
    @Override
    public String getBlink() {
        // TODO Auto-generated method stub
        //throw new UnsupportedOperationException("Unimplemented method 'getBlink'");
        long time = System.currentTimeMillis();
        long diff = time -this.lastBlinkTime;
        if (diff > setup.getBlinkMillis()) {
            blink = !blink;
            this.lastBlinkTime = time;
        }
        return String.valueOf(blink);
    }

    @Override
    public String getCounter() {
        // TODO Auto-generated method stub
       // throw new UnsupportedOperationException("Unimplemented method 'getCounter'");
       return String.valueOf(counter.getAndIncrement());
    }

    @Override
    public String getRandom() {
        // TODO Auto-generated method stub
        //throw new UnsupportedOperationException("Unimplemented method 'getRandom'");

         this.random = Math.round(Math.random()*setup.getMaxRandom());
         return String.valueOf(this.random);
    }

    @Override
    public void refresh() {
        // TODO Auto-generated method stub
        //throw new UnsupportedOperationException("Unimplemented method 'refresh'");
        this.getBlink();
        this.getRandom();
        this.getCounter();
    }

    public static void main(String[] args) {
        Simulator sim = new SimulatorImpl();
        
        System.out.println(sim);
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(sim);

    }
    
}
