package pt.deloitte.springiot.mqtt;

import org.springframework.context.ApplicationListener;
import org.springframework.integration.mqtt.event.MqttSubscribedEvent;
import org.springframework.stereotype.Component;

@Component
public class MqttSubscriber  implements ApplicationListener<MqttSubscribedEvent>{

    @Override
    public void onApplicationEvent(MqttSubscribedEvent event) {
        // TODO Auto-generated method stub
        //throw new UnsupportedOperationException("Unimplemented method 'onApplicationEvent'");
        System.out.println("Subscribe ok " + event.getMessage());
    }
    
}
