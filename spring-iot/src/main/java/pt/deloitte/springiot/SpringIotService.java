package pt.deloitte.springiot;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.Getter;



@Service
@Getter
public class SpringIotService {

    @Value("${app.name}")
    private String name;
    @Value("${app.version}")
    private String version;
    @Value("${app.description}")
    private String description;
    @Value("${spring.datasource.url}")
    private String database;
    
    @SuppressWarnings("unused")
    public String getChangedDatabase(){

        String[] parts = database.split(";");

        return parts[0];
    }

}
