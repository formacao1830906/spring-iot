package pt.deloitte.springiot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class SprintIotController {
    
    @Autowired
    SpringIotService props;
    
    @Autowired
    SpringIotProfile profile;

    @RequestMapping(value="/")
    public String index(Model m) {
        m.addAttribute("name", props.getName());
        m.addAttribute("version", props.getVersion());
        m.addAttribute("description", props.getDescription());
        m.addAttribute("profile", profile.getActiveProfile());
        m.addAttribute("database", props.getChangedDatabase());
        return "index";
    }
}
